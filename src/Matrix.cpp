#include "Matrix.h"
#include <iostream>
#include <vector>

using std::vector;
using std::ostream;


ostream& operator<<(ostream& stream, Matrix& matrix) {
    for (int i = 0; i < matrix.row; i++) {
        for (int j = 0; j < matrix.col; j++) {
            if (matrix.getValue(i, j) == matrix.getEmptyValue()) {
                stream.width(5);
                stream << "/  ";
            }
            else {
                stream.width(4);
                stream << matrix.getValue(i, j) << " ";
            }
        }
        stream << std::endl;
    }
    return stream;
}

Matrix::Matrix(int nbRows, int nbColumns, int emptyValue){
    this->data = vector<vector<int> >(nbRows, std::vector<int>(nbColumns, emptyValue));
    this->row = nbRows;
    this->col = nbColumns;
    this->emptyVal = emptyValue;
}

Matrix::~Matrix() {

}

void Matrix::addRow(int rowIndex) {
    typename vector<vector<int> >::iterator it = this->data.begin() + rowIndex;
    this->data.insert(it, vector<int>(this->col, this->emptyVal));
    this->row++;
}

void Matrix::removeRow(int rowIndex) {
    typename vector<vector<int> >::iterator it = this->data.begin() + rowIndex;
    this->data.erase(it);
    this->row--;
}

void Matrix::addColumn(int colIndex) {
    for (int row = 0; row < this->row; row++) {
        typename vector<int>::iterator it = this->data[row].begin() + colIndex;
        this->data[row].insert(it, this->emptyVal);
    }
    this->col++;
}

void Matrix::removeColumn(int colIndex) {
    for (int row = 0; row < this->row; row++) {
        typename vector<int>::iterator it = this->data[row].begin() + colIndex;
        this->data[row].erase(it);
    }
    this->col--;
}

int Matrix::getValue(int rowIndex, int colIndex) {
    return this->data[rowIndex][colIndex];
}

void Matrix::setValue(int rowIndex, int colIndex, int value) {
    this->data[rowIndex][colIndex] = value;
}