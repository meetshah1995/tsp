#include "tsplib.h"
#include <ctime>

using std::cout;
using std::endl;

string getInputParam(string cmd, int argc, char** argv) 
{
    for (int i = 0; i < argc; i++) 
    {
        if (argv[i] == cmd && i < argc - 1) 
        {
            return argv[i + 1];
        }
    }
    return "";
}

int main(int argc, char** argv) {
    #ifdef DEBUG
    std::clock_t start = std::clock();
    #endif

    string inputParam = getInputParam("-i", argc, argv);
    string outputParam = getInputParam("-o", argc, argv);
    if (inputParam == "") 
    {
        cout << "No input file" << endl;
        return -1;
    }

    ifstream inputFile(inputParam);

    Tsplib tsp(inputFile);
    inputFile.close();

    #ifdef DEBUG
    double duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
    cout << "Total Running Time : " << duration << " (seconds)" << endl;
    #endif
    
    return 0;
}