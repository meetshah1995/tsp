#ifndef MATRIX_H
#define	MATRIX_H

#include <iostream>
#include <vector>

using std::vector;
using std::ostream;

class Matrix;
ostream& operator<<(ostream&, Matrix&);

class Matrix {
private:
    int row;
    int col;
    int emptyVal;
    vector<vector<int> > data;
    
public:
    Matrix(int nbRows = 0, int nbColumns = 0, int emptyValue = 0);

    int getEmptyValue() { return this->emptyVal; }

    int getNbRows() { return this->row; }
    void addRow(int rowIndex);
    void removeRow(int rowIndex);

    int getNbColumns() { return this->col; }
    void addColumn(int colIndex);
    void removeColumn(int colIndex);

    int getValue(int rowIndex, int colIndex);
    void setValue(int rowIndex, int colIndex, int value);

    ~Matrix();
    
    friend ostream& operator<<(ostream&, Matrix&);
};

#endif