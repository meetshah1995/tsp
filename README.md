## Little et.al BNB TSP Algorithm Implementation | C++11

## Meet Shah | 13d070003 | meetshah1995@ee.iitb.ac.in

### Implementation Details

* A class Little which has all the datastructures as requested in the problem statement. 
* I've fairly well documented this class to clarify what exactly each unit of the code does.
* BnB was implemented using simple deques.
* Code is std::c++11 compliant.
* Matrix.h from (see below) Bjarne's webpage was a template based code. Since we only were looking for integer values, I untemplated the code to `int32` and removed a lot of sanity and exception checks to marginally improve performance.

### Running the code

* Simple enter the `src` directory and run `./make_run.sh`
* It will load a standard 17 node tsp and print the solution.

### My Code:

* Little.h and Little.cpp and main.cpp files were completely written by me.
* All debugging and stages of the coding can be seen in my bitbucket commit history.

### External Utility Code used : 

* Famous TSPLib.cpp which has utlity functions to read and write standard `.tsp` configuration files and print solutions.
* Basic Matrix class from Bjarne Stroustrup's webiste.

### Code Development and Commit History

Please refer to my repository commits online for details on the code additions as I wrote it: 


